+++
title =  "The All New Version 5.1"
date =   "2023-01-18"
description = "A Complete Redesign"
+++

I completely redesigned my site and i love it


![](/media/new_design.png)

The new features are:

- New Home & Blog & Projects designs.
- Less dependency on JS. (outside of RSS feed)
- More ways to add easter eggs.
- Now using local fonts instead of bunnyfonts. 
- New font [JuliaMono.](https://github.com/cormullion/juliamono/)

And thats it hope you have a great day <3