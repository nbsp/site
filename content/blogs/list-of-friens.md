+++
title =  "List of best friends"
date =  "2022-11-09"
description = "Many best friends in my discord :3"
+++

###### This is of course a non-exhaustive list. 
###### if you think you should be here just hit a dm

Many great friends have been in my part of my discord and revolt journey and this is a way to honor them and thank them. (this is based on <a href="https://infi.sh/post/based-people">infi's list</a>)

- <a href="https://waitwhat.sh">Wait What</a>
- <a href="https://infi.sh/">Infi</a>
- <a href="https://soafen.love">Soafen</a>
- <a href="https://github.com/initmd">Geshi</a>
- <a href="https://singleslice.github.io/">Cake</a>
- <a>Lumi</a>
- <a href="https://pinchese.github.io/">Penny</a>
- <a href="https://github.com/axtloss">Axtlos (neko arc enjoyer)</a>
- <a>Cara</a>
- <a href="https://mayu.gay/">Mayu</a>
- <a href="https://github.com/rexogamer">Rexo</a>
- <a href="https://github.com/Selphy1337">Selphy</a>
- <a href="https://second2050.me">Second</a>
- <a href="https://theevilskeleton.gitlab.io/">TheEvilSkelly</a>
- <a href="https://github.com/leafty">Flora</a>
