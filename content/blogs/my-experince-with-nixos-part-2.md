+++
title =  "My experince with NixOS part 2"
date =  "2022-12-12"
description = "my experince with NixOS part 2"
image = "/media/my_config.png"
+++

since my last blog post i gotten better at Nixos and and there is some good news about that
which is i rewrote my whole config for nixos to be completely modular and i love it bc its now much easier to maintain and update instead of my sorta very manual config

![](/media/my_config.png) [my new updated config](https://codeberg.org/oomfie/dotfiles)

and i made few updates now instead of having to manually copying configuration.nix
i just made the flake.nix file just grab the default (made by install) configuration.nix
and i made my dns work on nixos (or in linux for the first time) and thank u for reading hope u have a great day <3