<div align="center">
<img src="https://codeberg.org/oomfie/site/raw/branch/main/logo.svg" width="200">
  <h1 align="center">Rewrite V6</h1>
</div>

<div align="center">
  <a href="https://codeberg.org/oomfie/site/src/branch/main/LICENSE">
    <img src="https://img.shields.io/static/v1?label=License&message=AGPL-3&color=1e1e20&style=flat-square">
  </a>
  <br>
  <hr />

</div>

## Install
Be sure to have all [you need](https://www.getzola.org/documentation/getting-started/installation/) before running anything.

[check the getstarted guide](https://www.getzola.org/documentation/getting-started/cli-usage/)
